import { https } from "./configURL";

export const getRoomInfor = (id) => {
  return https.get(`/api/phong-thue/${id}`);
};

export const getLocation = (id) => {
  return https.get(`/api/vi-tri/${id}`);
};

export const getComments = (id) => {
  return https.get(`/api/binh-luan/lay-binh-luan-theo-phong/${id}`);
};
