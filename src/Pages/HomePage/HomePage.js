import Home from 'Pages/Home/Home';
import React, { Component } from 'react'
import MenuSilder from './../../Slider/MenuSilder';

export default class HomePage extends Component {
    render() {
        return (
            <div>
                <MenuSilder />
                <Home />
            </div>
        )
    }
}
