import { userLocalService } from "../Pages/Services/LocalService";
import { RES_USER_LOGIN, SET_USER_LOGIN } from "./constants/UserContants";

const initialState = {
  user: userLocalService.get(),
  register: {
    id: 0,
    name: [],
    email: [],
    password: [],
    phone: [],
    birthday: [],
    gender: true,
    role: [],
  },
};

export const UserReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_USER_LOGIN:
      return { ...state, user: payload };
    case RES_USER_LOGIN:
      return { ...state, register: payload };
    default:
      return state;
  }
};
