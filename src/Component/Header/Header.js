import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";
import { FaAirbnb } from "react-icons/fa";
import MidNav from "./MidNav";

export default class Header extends Component {
  render() {
    return (
      <div className="flex justify-between items-center px-20 py-5">
        <NavLink to={"/"}>
          <div className="flex">
            <FaAirbnb className="text-5xl pt-1 text-green-700 font-light" />
            <span className="text-3xl pt-1 text-green-700 font-medium">
              airbnb
            </span>
          </div>
        </NavLink>
        <MidNav />
        <UserNav />
      </div>
    );
  }
}
