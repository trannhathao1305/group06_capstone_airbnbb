import React, { Component } from "react";
import { FaGlobe } from "react-icons/fa";
import MenuTab from "../../Slider/MenuTab";
export default class UserNav extends Component {
  render() {
    return (
      <div className="flex items-center justify-end">
        <div className="flex mr-8 items-center z-10 ">
          <a href="" className="hover:bg-slate-500 px-4 py-2 rounded-full">
            Trở thành chủ nhà
          </a>
          <FaGlobe className=" p-3 hover:bg-slate-500 h-10 w-10 rounded-full" />
        </div>
        <div>
          <MenuTab />
        </div>
      </div>
    );
  }
}
